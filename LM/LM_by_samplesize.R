# Logistic Map example by sample size
# workingdir = "C:/Users/Sangwon/Dropbox/CMU/courses(CURRENT)/[research]IndirInference/examples"
# setwd(workingdir)

## readme: 
## this is a wrapper for MA1, to run the same simulations over different sample sizes, and save+load+plot. It is meant for a command line run.
source("LM_config.R")
source("LM_functions.R")

if(!file.exists(filename)){
  theta_hat_param_list = list()
  theta_hat_nonparam_list = list()
  for(ss in 1:length(len_fixed_list)){
    len_fixed = len_fixed_list[ss]
    cat(fill=T)
    cat("sample size ", len_fixed, fill=T)
    print(Sys.time())
    source("MA1.R")
    theta_hat_param_list[[ss]] = list(len = len_fixed, theta_hat_param = theta_hat_param, theta_truth = theta_truth)
    theta_hat_nonparam_list[[ss]] = list(len = len_fixed, theta_hat_nonparam = theta_hat_nonparam, theta_truth = theta_truth)
  }
  save(list = c("theta_hat_param_list", "theta_hat_nonparam_list", "len_fixed_list"), file = filename)
}


## plot something.
load(filename)
#example:
# len_fixed_list = 1:3
# for(kk in 1:3) theta_hat_param_list[[kk]] = list(len=kk, theta_hat_param = (1:100)+(5*k), theta_truth = 1)
# for(kk in 1:3) theta_hat_nonparam_list[[kk]] = list(len=kk, theta_hat_nonparam = (1:100)+(7*k), theta_truth = 1)

plotsim(theta_hat_param_list, theta_hat_nonparam_list, len_fixed_list = len_fixed_list, plotname = "MA1_sim.pdf", height = 10, width = 15)