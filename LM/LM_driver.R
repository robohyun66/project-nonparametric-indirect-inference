# Logistic Map example by sample size
# workingdir = "C:/Users/Sangwon/Dropbox/CMU/courses(CURRENT)/[research]IndirInference/examples"
# setwd(workingdir)

## readme: 
## this is a wrapper for MA1, to run the same simulations over different sample sizes, and save+load+plot. It is meant for a command line run.
source("LM_config.R")
source("LM_functions.R")
if(!file.exists(filename)){
  theta_hat_param_list = list()
  theta_hat_nonparam_list = list()
  
  for(ss in 1:length(len_list)){
    cat(fill=T)
    len = len_list[ss]
    
    ## fixed, original data
    dat_fixed = gendat(theta_truth, sigma, len)
      
    beta_dat = auxvar(dat_fixed) 
    beta_dat2 = auxvar2(dat_fixed, smoother = smoother) ## change to smoothing spline and do it again 
    
    ## simulated data
    cat("sample size: ", len, fill=T)
    print(Sys.time())
    
    theta_hat_param    = gentheta(nsim, theta_grid, sigma, len, beta_dat,  type = "param")
    theta_hat_nonparam = gentheta(nsim, theta_grid, sigma, len, beta_dat2, type = "nonparam")
    
    theta_hat_param_list[[ss]]    = list(len = len, theta_hat_param    = theta_hat_param, theta_truth    = theta_truth)
    theta_hat_nonparam_list[[ss]] = list(len = len, theta_hat_nonparam = theta_hat_nonparam, theta_truth = theta_truth)
  }
  save(list = c("theta_hat_param_list", "theta_hat_nonparam_list", "len_fixed_list"), file = filename)
}


## plot something.
load(filename)
plotsim(theta_hat_param_list, theta_hat_nonparam_list, len_list = len_list, plotname = "MA1_sim.pdf", height = 10, width = 15)