### Setup
## Auxiliary model: x,y are independent poisson:
## Bivariate indep normal RV's, with mean theta and cov I

## original data : 
set.seed(5)
ndat = 10
theta = c(3,8)
x = rnorm( ndat, theta[1], 1  )
y = rnorm( ndat, theta[2], 1  )
dat = cbind( x , y)

auxvar_orig =  apply(dat, 2, mean)  ## should be the MLE of the Poisson variable, from the data

## range of parameters
n_cands = 20
theta_cands1 = seq( from = 0, to = 10, length = n_cands )
theta_cands2 = seq( from = 0, to = 10, length = n_cands )

## 1. for each of parameter candidates, generate GAUSSIAN data, then collect the auxil param of that dataset
auxvar_sim_list = matrix( ncol = 2, nrow = n_cands^2)
mm = 1
for(kk in 1:n_cands){
  for(pp in 1:n_cands){
    sim_dat = cbind( rnorm ( ndat, theta_cands1[kk] , 1),
                     rnorm ( ndat, theta_cands2[pp] , 1) )
    auxvar_sim = apply(sim_dat, 2, mean) ## MLE of the SIMULATED Poisson     
    auxvar_sim_list[mm,] = auxvar_sim
    mm = mm + 1
  }
}

## find which guy in auxvar_sim_list that was most similar to auxvar_orig
auxvar_orig_mat = matrix(rep(auxvar_orig,each=n_cands^2),nrow=n_cands^2)
gotcha = which.min(apply( (auxvar_sim_list - auxvar_orig_mat)^2, 1, sum))

## indirect estimators
theta_cands1[ floor( gotcha / n_cands ) + 1]
theta_cands2[ gotcha %% n_cands ]