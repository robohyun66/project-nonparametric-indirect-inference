simname = "MA1_sim_kernel.gcv"
filename = paste0(simname, ".Rdata")
plotname = paste0(simname, ".pdf")

## settings:
sigma = 1
nsim = 100 # fix this

theta_truth =  .8# real param
theta_grid = seq(from=0.01,to=1,length=100) # the grid of theta you choose to simulate from

len_list = c(10, 20, 50, 100, 200, 500, 1000, 5000, 10000)
smoother = "kernel.gcv" # "kernel"